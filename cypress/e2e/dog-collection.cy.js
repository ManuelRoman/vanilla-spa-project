describe("template spec", () => {
  it("passes", () => {
    cy.visit("http://localhost:8080");
    cy.get(":nth-child(2) > .ul__li--style").click();
    cy.get(".main-header__logo-image").click();
    cy.get(":nth-child(2) > .ul__li--style").click();
    cy.get(":nth-child(1) > .ul__li--style").click();
    cy.get(":nth-child(2) > .ul__li--style").click();
    cy.get(".dog-list__section > :nth-child(1)").click();
    cy.get(".dog-page__wrapper > :nth-child(1)").click();
    cy.get(".dog-page__wrapper > :nth-child(2)").click();
    cy.get(":nth-child(2) > .ul__li--style").click();

    function findBreed(targetSelector) {
      cy.get(".dog-list__section")
        .then(($container) => {
          if ($container.find(targetSelector).length) {
            return true;
          } else {
            return false;
          }
        })
        .then((found) => {
          if (found) {
            return cy.get(targetSelector).click();
          } else {
            if (Cypress.$(".dog-list__wrapper > :nth-child(3)").length) {
              cy.get(".dog-list__wrapper > :nth-child(3)").click();
              return findBreed(targetSelector);
            } else {
              throw new Error(
                `Couldn't find elem "${targetSelector}" on any page`
              );
            }
          }
        });
    }

    findBreed('[breed="hound"] > .breed-card__a');

    cy.get(".dog-page__ul > :nth-child(1)").click();
    cy.get(".dog-page__wrapper > :nth-child(1)").click();
    cy.get(".dog-page__ul > :nth-child(2)").click();
    cy.get(".dog-page__wrapper > :nth-child(2)").click();

    cy.get(".main-header__logo-image").click();
  });
});
