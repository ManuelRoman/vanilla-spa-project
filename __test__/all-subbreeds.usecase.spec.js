import { DogsRepository } from "../src/repositories/dogs.repository";
import { AllSubBreedsUseCase } from "../src/usecases/all-subbreeds.usecase";
import { HOUND_SUB_BREEDS } from "./fixtures/houndSubBreeds";

jest.mock("../src/repositories/dogs.repository");

describe("Get dog subbreed list", () => {
  beforeEach(() => {
    DogsRepository.mockClear();
  });

  it("Should run", async () => {
    DogsRepository.mockImplementation(() => {
      return {
        getAllSubBreeds: () => {
          return HOUND_SUB_BREEDS;
        },
      };
    });

    const breed = "hound";

    const dogSubBreedList = await AllSubBreedsUseCase.execute(breed);

    expect(dogSubBreedList.status).toBe(HOUND_SUB_BREEDS.status);

    expect(Object.keys(dogSubBreedList.message).length).toBe(
      Object.keys(HOUND_SUB_BREEDS.message).length
    );
  });
});
