import { DogsRepository } from "../src/repositories/dogs.repository";
import { AllBreedsUseCase } from "../src/usecases/all-breeds.usecase";
import { ALL_BREEDS } from "./fixtures/allBreeds";

jest.mock("../src/repositories/dogs.repository");

describe("Get dog breed list", () => {
  beforeEach(() => {
    DogsRepository.mockClear();
  });

  it("Should run", async () => {
    DogsRepository.mockImplementation(() => {
      return {
        getAllBreeds: () => {
          return ALL_BREEDS;
        },
      };
    });

    const dogBreedList = await AllBreedsUseCase.execute();

    console.log(dogBreedList);

    expect(dogBreedList.status).toBe(ALL_BREEDS.status);

    expect(Object.keys(dogBreedList.message).length).toBe(
      Object.keys(ALL_BREEDS.message).length
    );
  });
});
