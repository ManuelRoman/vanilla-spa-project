import { DogsRepository } from "../src/repositories/dogs.repository";
import { SingleRandomSubBreedImageUseCase } from "../src/usecases/single-random-subbreed-image.usecase";
import { RANDOM_SUBBREED_IMAGE } from "./fixtures/singleRandomSubBreedImage";

jest.mock("../src/repositories/dogs.repository");

describe("Get single random dog breed image", () => {
  beforeEach(() => {
    DogsRepository.mockClear();
  });

  it("Should run", async () => {
    DogsRepository.mockImplementation(() => {
      return {
        getSingleRandomSubBreedImage: () => {
          return RANDOM_SUBBREED_IMAGE;
        },
      };
    });

    const breed = "hound";
    const subBreed = "afghan";

    const singleSubBreedImage = await SingleRandomSubBreedImageUseCase.execute(
      breed,
      subBreed
    );

    expect(singleSubBreedImage.status).toBe(RANDOM_SUBBREED_IMAGE.status);

    expect(singleSubBreedImage.message).toBe(RANDOM_SUBBREED_IMAGE.message);
  });
});
