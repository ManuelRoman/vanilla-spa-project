export const RANDOM_BREED_IMAGES = {
  message: [
    "https://images.dog.ceo/breeds/hound-basset/n02088238_10054.jpg",
    "https://images.dog.ceo/breeds/hound-blood/n02088466_8842.jpg",
    "https://images.dog.ceo/breeds/hound-walker/n02089867_148.jpg",
  ],
  status: "success",
};
