export const RANDOM_DOG_IMAGES = {
  message: [
    "https://images.dog.ceo/breeds/collie-border/n02106166_7282.jpg",
    "https://images.dog.ceo/breeds/havanese/00100trPORTRAIT_00100_BURST20191112123933390_COVER.jpg",
    "https://images.dog.ceo/breeds/dingo/n02115641_6639.jpg",
  ],
  status: "success",
};
