export const HOUND_SUB_BREEDS = {
  message: [
    "afghan",
    "basset",
    "blood",
    "english",
    "ibizan",
    "plott",
    "walker",
  ],
  status: "success",
};
