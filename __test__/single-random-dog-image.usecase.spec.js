import { DogsRepository } from "../src/repositories/dogs.repository";
import { SingleRandomDogImageUseCase } from "../src/usecases/single-random-dog-image.usecase";
import { RANDOM_DOG_IMAGE } from "./fixtures/singleRandomDogImage";

jest.mock("../src/repositories/dogs.repository");

describe("Get single random dog image", () => {
  beforeEach(() => {
    DogsRepository.mockClear();
  });

  it("Should run", async () => {
    DogsRepository.mockImplementation(() => {
      return {
        getSingleRandomDogImage: () => {
          return RANDOM_DOG_IMAGE;
        },
      };
    });

    const ranDogImage = await SingleRandomDogImageUseCase.execute();

    expect(ranDogImage.status).toBe(RANDOM_DOG_IMAGE.status);

    expect(ranDogImage.message).toBe(RANDOM_DOG_IMAGE.message);
  });
});
