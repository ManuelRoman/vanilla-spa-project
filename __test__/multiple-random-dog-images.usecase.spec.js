import { DogsRepository } from "../src/repositories/dogs.repository";
import { MultipleRandomDogImagesUseCase } from "../src/usecases/multiple-random-dog-images.usecase";
import { RANDOM_DOG_IMAGES } from "./fixtures/multipleRandomDogImages";

jest.mock("../src/repositories/dogs.repository");

describe("Get multiple random dog images", () => {
  beforeEach(() => {
    DogsRepository.mockClear();
  });

  it("Should run", async () => {
    DogsRepository.mockImplementation(() => {
      return {
        getMultipleRandomDogImages: () => {
          return RANDOM_DOG_IMAGES;
        },
      };
    });

    const number = 3;

    const ranDogImages = await MultipleRandomDogImagesUseCase.execute(number);

    expect(ranDogImages.status).toBe(RANDOM_DOG_IMAGES.status);

    expect(ranDogImages.message[0]).toBe(RANDOM_DOG_IMAGES.message[0]);

    expect(ranDogImages.message.length).toBe(RANDOM_DOG_IMAGES.message.length);
  });
});
