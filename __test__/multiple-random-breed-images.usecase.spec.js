import { DogsRepository } from "../src/repositories/dogs.repository";
import { MultipleRandomBreedImagesUseCase } from "../src/usecases/multiple-random-breed-images.usecase";
import { RANDOM_BREED_IMAGES } from "./fixtures/multipleRandomBreedImages";

jest.mock("../src/repositories/dogs.repository");

describe("Get multiple random dog breed images", () => {
  beforeEach(() => {
    DogsRepository.mockClear();
  });

  it("Should run", async () => {
    DogsRepository.mockImplementation(() => {
      return {
        getMultipleRandomBreedImages: () => {
          return RANDOM_BREED_IMAGES;
        },
      };
    });

    const breed = "hound";
    const number = 3;

    const multipleBreedImages = await MultipleRandomBreedImagesUseCase.execute(
      breed,
      number
    );

    console.log(multipleBreedImages);

    expect(multipleBreedImages.status).toBe(RANDOM_BREED_IMAGES.status);

    expect(multipleBreedImages.message[0]).toBe(RANDOM_BREED_IMAGES.message[0]);

    expect(multipleBreedImages.message.length).toBe(
      RANDOM_BREED_IMAGES.message.length
    );
  });
});
