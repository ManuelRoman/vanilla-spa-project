FROM node:18.10.0-alpine3.16 AS builder
RUN mkdir /webpack-app
WORKDIR /webpack-app
COPY . .
RUN npm ci
RUN npm run build

FROM nginx:1.13.3-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /webpack-app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]