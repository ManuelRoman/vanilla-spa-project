import { Router } from "@vaadin/router";
import { state } from "./states/state";
import "./main.css";
import "./pages/home.page";
import "./pages/dog-list.page";
import "./pages/dog.page.js";

const outlet = document.querySelector("#outlet");
const router = new Router(outlet);

router.setRoutes([
  {
    path: "/",
    component: "home-page",
    action: () => {
      state.breed = "";
      state.subBreed = "";
      state.singleImage = "";
      state.multiImage = [];
    },
  },
  {
    path: "/breed/list",
    component: "dog-list",
    action: () => {
      state.breed = "";
      state.subBreed = "";
      state.singleImage = "";
      state.multiImage = [];
    },
  },
  {
    path: "/breed/:breedName",
    component: "dog-page",
    action: (context) => {
      state.breed = context.params.breedName;
    },
  },
  { path: "(.*)", redirect: "/" },
]);
