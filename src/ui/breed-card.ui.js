import { LitElement, html } from "lit";
import { SingleRandomBreedImageUseCase } from "../usecases/single-random-breed-image.usecase";
import { state } from "../states/state";
import { sentenceCase } from "../utils/utils";

export class BreedUI extends LitElement {
  static get properties() {
    return {
      breed: { type: String },
      dogImage: { type: String },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.dogImage = "";
    this.dogImage = await SingleRandomBreedImageUseCase.execute(this.breed);
  }

  async updated(changedProperties) {
    if (changedProperties.has("breed")) {
      this.dogImage = "";
      await SingleRandomBreedImageUseCase.execute(this.breed).then((result) => {
        this.dogImage = result;
      });
    }
  }

  render() {
    return html`
      <a href="/breed/${this.breed}" class="breed-card__a">
        <p class="breed-card__p">${sentenceCase(this.breed)}</p>
        <img
          class="breed-card__img"
          src="${this.dogImage.message}"
          alt="${this.breed}"
          onerror="this.src='${state.errorImage}';"
        />
      </a>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("breed-ui", BreedUI);
