import { DogsRepository } from "../repositories/dogs.repository";

export class MultipleRandomBreedImagesUseCase {
  static async execute(breed, number) {
    const repository = new DogsRepository();
    return await repository.getMultipleRandomBreedImages(breed, number);
  }
}
