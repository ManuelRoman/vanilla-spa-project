import { DogsRepository } from "../repositories/dogs.repository";

export class SingleRandomSubBreedImageUseCase {
  static async execute(breed, subBreed) {
    const repository = new DogsRepository();
    return await repository.getSingleRandomSubBreedImage(breed, subBreed);
  }
}
