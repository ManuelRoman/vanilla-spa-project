import { DogsRepository } from "../repositories/dogs.repository";

export class MultipleRandomDogImagesUseCase {
  static async execute(number) {
    const repository = new DogsRepository();
    return await repository.getMultipleRandomDogImages(number);
  }
}
