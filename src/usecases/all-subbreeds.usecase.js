import { DogsRepository } from "../repositories/dogs.repository";

export class AllSubBreedsUseCase {
  static async execute(breed) {
    const repository = new DogsRepository();
    return await repository.getAllSubBreeds(breed);
  }
}
