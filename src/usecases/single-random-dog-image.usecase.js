import { DogsRepository } from "../repositories/dogs.repository";

export class SingleRandomDogImageUseCase {
  static async execute() {
    const repository = new DogsRepository();
    return await repository.getSingleRandomDogImage();
  }
}
