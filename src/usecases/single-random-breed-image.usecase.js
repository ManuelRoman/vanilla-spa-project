import { DogsRepository } from "../repositories/dogs.repository";

export class SingleRandomBreedImageUseCase {
  static async execute(breed) {
    const repository = new DogsRepository();
    return await repository.getSingleRandomBreedImage(breed);
  }
}
