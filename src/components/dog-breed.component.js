import { LitElement, html } from "lit";
import { state } from "../states/state";
import { subscribe } from "valtio";
import { MultipleRandomBreedImagesUseCase } from "../usecases/multiple-random-breed-images.usecase";
import { SingleRandomBreedImageUseCase } from "../usecases/single-random-breed-image.usecase";
import { MultipleRandomSubBreedImagesUseCase } from "../usecases/multiple-random-subbreed-images.usecase";
import { SingleRandomSubBreedImageUseCase } from "../usecases/single-random-subbreed-image.usecase";

export class DogBreed extends LitElement {
  constructor() {
    super();
    subscribe(state, () => this.requestUpdate());
  }

  static get properties() {
    return {
      option: { type: Boolean },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.option = true;
    state.singleImage = await SingleRandomBreedImageUseCase.execute(
      state.breed
    );
  }

  render() {
    return html`
      <div class="dog-page__wrapper">
        <button @click="${() => this.handleSingleImage()}" class="btn">
          Single Image
        </button>
        <button @click="${() => this.handleMultipleImage()}" class="btn">
          Multiple Images
        </button>
      </div>
      ${this.option
        ? html` <section class="dog-page__section">
            <img
              class="dog-page__single-img sketchy"
              src="${state.singleImage.message}"
              onerror="this.src='${state.errorImage}';"
              alt="${state.subBreed !== "" &&
              "${state.subBreed}"} ${state.breed}"
            />
          </section>`
        : html`
            <section class="dog-page__section">
              ${state.multiImage.message?.map((image, index) => {
                return html`<img
                  class="dog-page__multi-img sketchy"
                  src="${image}"
                  onerror="this.src='${state.errorImage}';"
                  alt="${state.subBreed !== "" &&
                  "${state.subBreed}"} ${state.breed} ${index}"
                />`;
              })}
            </section>
          `}
    `;
  }

  async handleSingleImage() {
    this.option = true;
    if (state.subBreed === "") {
      state.singleImage = await SingleRandomBreedImageUseCase.execute(
        state.breed
      );
    } else {
      state.singleImage = await SingleRandomSubBreedImageUseCase.execute(
        state.breed,
        state.subBreed
      );
    }
    state.multiImage = [];
  }

  async handleMultipleImage() {
    this.option = false;
    if (state.subBreed === "") {
      state.multiImage = await MultipleRandomBreedImagesUseCase.execute(
        state.breed,
        3
      );
    } else {
      state.multiImage = await MultipleRandomSubBreedImagesUseCase.execute(
        state.breed,
        state.subBreed,
        3
      );
    }

    state.singleImage = "";
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("dog-breed", DogBreed);
