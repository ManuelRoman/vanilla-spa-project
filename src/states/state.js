import { proxy } from "valtio";

export const state = proxy({
  breed: "",
  subBreed: "",
  singleImage: "",
  multiImage: [],
  errorImage:
    "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Dog_silhouette.svg/800px-Dog_silhouette.svg.png",
});
