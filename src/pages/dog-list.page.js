import { LitElement, html } from "lit";
import { AllBreedsUseCase } from "../usecases/all-breeds.usecase";
import "../ui/breed-card.ui";

export class DogList extends LitElement {
  static get properties() {
    return {
      dogList: { type: Object },
      breed: { type: String },
      page: { type: Number },
      itemsPerPage: { type: Number },
      shuffleDogList: { type: Array },
      prevButtonDisabled: { type: Boolean },
      nextButtonDisabled: { type: Boolean },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.dogList = {};
    this.dogList = await AllBreedsUseCase.execute();
    this.page = 1;
    this.itemsPerPage = 10;
    this.shuffledDogList = Object.keys(this.dogList.message).sort(
      () => Math.random() - 0.5
    );
    this.prevButtonDisabled = true;
    this.nextButtonDisabled =
      Math.ceil(
        Object.keys(this?.dogList?.message).length / this.itemsPerPage
      ) <= 1;
  }

  render() {
    const startIndex = (this.page - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    const breeds = this.shuffledDogList?.slice(startIndex, endIndex);

    return html`
      <h1 class="dog-list__h1">Dog List</h1>
      <div class="dog-list__wrapper">
        <button
          class="btn"
          @click="${this.handlePreviousPage}"
          ?disabled="${this.prevButtonDisabled}"
        >
          Previous
        </button>
        <span>${this.page}</span>
        <button
          class="btn"
          @click="${this.handleNextPage}"
          ?disabled="${this.nextButtonDisabled}"
        >
          Next
        </button>
      </div>
      <section class="dog-list__section">
        ${breeds?.map(
          (breed, index) =>
            html`<breed-ui
              breed="${breed}"
              key="${index}"
              class="dog-list__dog-card"
            ></breed-ui>`
        )}
      </section>
    `;
  }

  handlePreviousPage() {
    if (this.page > 1) {
      this.page -= 1;
      this.prevButtonDisabled = this.page <= 1;
      this.nextButtonDisabled = false;
      this.requestUpdate();
    }
  }

  handleNextPage() {
    const totalPages = Math.ceil(
      Object.keys(this?.dogList?.message).length / this.itemsPerPage
    );
    if (this.page < totalPages) {
      this.page += 1;
      this.prevButtonDisabled = false;
      this.nextButtonDisabled = this.page >= totalPages;
      this.requestUpdate();
    }
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("dog-list", DogList);
