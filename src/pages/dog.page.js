import { LitElement, html } from "lit";
import { state } from "../states/state";
import { subscribe } from "valtio";
import { sentenceCase } from "../utils/utils";
import "../components/dog-breed.component";
import { AllSubBreedsUseCase } from "../usecases/all-subbreeds.usecase";

export class Dog extends LitElement {
  constructor() {
    super();
    subscribe(state, () => this.requestUpdate());
  }

  static get properties() {
    return {
      subBreedList: { type: Object },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.subBreedList = { message: [], status: "error" };
    this.subBreedList = await AllSubBreedsUseCase.execute(state.breed);
  }

  render() {
    return html`
      <h1
        @click="${() => this.handleUpdateSubBreed("")}"
        class="dog-page__h1"
        title="${state.subBreed !== ""
          ? "Click me to reset the sub-breed"
          : ""}"
        alt="${state.subBreed !== "" ? "Click me to reset the sub-breed" : ""}"
      >
        ${sentenceCase(state.breed)}
      </h1>
      ${this?.subBreedList.message.length > 0
        ? html`
            <ul class="dog-page__ul">
              ${this.subBreedList.message.map((subBreed) => {
                return html`<li
                  class="dog-page__li ${state.subBreed === subBreed &&
                  "dog-page__li--selected"}"
                  @click="${() => {
                    this.handleUpdateSubBreed(subBreed);
                  }}"
                >
                  ${sentenceCase(subBreed)}
                </li>`;
              })}
            </ul>
          `
        : null}
      <dog-breed></dog-breed>
    `;
  }

  handleUpdateSubBreed(subBreed) {
    state.subBreed = subBreed;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("dog-page", Dog);
