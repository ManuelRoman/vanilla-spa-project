import { LitElement, html } from "lit";

export class HomePage extends LitElement {
  render() {
    return html`
      <h1 class="home__h1">
        Welcome to <span class="home-section__span">The Dog Collection</span>
      </h1>
      <section>
        <p class="home-section__p">
          This a learning Single Web Application (SPA), in which the
          <a
            class="home-section__a"
            href="https://dog.ceo/dog-api/documentation/"
            >DOG API</a
          >
          is used
        </p>
        <p class="home-section__p">
          Here you will find a list of many dog breeds, some sub-breeds and, of
          course, dog pictures!
        </p>
      </section>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("home-page", HomePage);
